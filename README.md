# Pixel Gen
A tool to generate random pixel art.

`git clone git@gitlab.com:jtrip/pixel-gen.git`

`cd pixel-gen`

`python3 -m venv venv`

`source venv/bin/activate`

`pip install -r requirements.txt`


## Two Fold
`two_fold.py` can be used to generate pixel art with two fold symmetry.

Default image size is 16x16, you can also specify a positive even integer.

`python two_fold.py 6`    
`python two_fold.py 1024` -- probably takes 2 seconds

If the pixel dimension is less than 32 we put the original quadrant in the name.
Otherwise we put a UUID4 in the name.