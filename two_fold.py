import sys
import uuid
from PIL import Image, ImageColor
from random import choice
from pathlib import Path


def sub_section(dim):
    """
    :param dim: pixel dimension int
    :return: 2 dimensional matrix of random bits
    [
    [1, 1, 0, 0],
    [0, 1, 0, 1],
    [1, 0, 1, 0],
    [0, 0, 0, 1],
    ]
    """

    matrix = []
    for x in range(dim):
        row = []
        for y in range(dim):
            row.append(choice((0, 1)))
        matrix.append(row)
    return matrix


# a brief version
def sub_section_b(dim):
    return [[choice((0, 1)) for y in range(dim)] for x in range(dim)]


def mirror_matrix(matrix, axis):
    if axis == 'x':
        return [r[::-1] for r in matrix]
    if axis == 'y':
        return matrix[::-1]


def paint_quadrant(matrix, image, offset, quadrant):
    """
    :param matrix:      Random bits in x*x list of lists.
    :param image:       Pillow Image object to paint on.
    :param offset:      Half the dimension of the Image object.
    :param quadrant:    Which quadrant this matrix should be applied to.
    :return: none

    Paints the matrix onto the quadrant of the image

    Quadrant positions:

    1 | 2
    -- --
    3 | 4

    """
    for row in range(offset):
        for col in range(offset):
            if matrix[row][col]:
                if quadrant == 1:
                    image.putpixel((row, col), ImageColor.getcolor('white', '1'))
                if quadrant == 2:
                    image.putpixel((row + offset, col), ImageColor.getcolor('white', '1'))
                if quadrant == 3:
                    image.putpixel((row, col + offset), ImageColor.getcolor('white', '1'))
                if quadrant == 4:
                    image.putpixel((row + offset, col + offset), ImageColor.getcolor('white', '1'))


def save_image(img, pixel_dim, pixels):
    # name/address, if less than 32 pixels we can put the bits in the name, otherwise uuid
    if pixel_dim < 32:
        address = ''.join([''.join( [str(x) for x in pixels[y]]) for y in range(len(pixels))])
    else:
        address = uuid.uuid4()

    output_dir = Path("output")
    if output_dir.is_dir():
        img.save(f'output/tf{pixel_dim}_{address}.png')
    else:
        output_dir.mkdir()
        img.save(f'output/tf{pixel_dim}_{address}.png')


def main(pixel_dim=16):
    # create the Image
    sub_dim = int(pixel_dim/2)

    image = Image.new('1', (pixel_dim, pixel_dim))

    pixels = sub_section(sub_dim)
    paint_quadrant(pixels, image, sub_dim, 1)

    y_mirror_pixels = mirror_matrix(pixels, 'y')
    paint_quadrant(y_mirror_pixels, image, sub_dim, 2)

    x_mirror_pixels = mirror_matrix(pixels, 'x')
    paint_quadrant(x_mirror_pixels, image, sub_dim, 3)

    xy_mirror_pixels = mirror_matrix(x_mirror_pixels, 'y')
    paint_quadrant(xy_mirror_pixels, image, sub_dim, 4)

    save_image(image, pixel_dim, pixels)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        try:
            dimension = int(sys.argv[1])
            main(dimension)
        except ValueError as e:
            print("If you want specify a pixel dimension supply a positive even integer.")
    else:
        main()
